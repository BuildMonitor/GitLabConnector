Connector =
{
    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "GitLab CI" },
        ServerUrl: { Type: "text", Title: "GitLab Server URL", Required: true, Restart: true, ProtocolCheck: true },
        Token: { Type: "text", Title: "Token", Required: true, Restart: true },
        ImportantRefs: { Type: "stringList", Title: "Important References (Branches/Tags)", Value: [ "master", "develop", "release/*" ] },
        Interval: { Type: "number", Title: "Request Interval (in milliseconds)", Value: 1000, Required: true }
    },

    Interval: 1000,

    Index: 0,

    _refreshTimer: null,
    _refreshing: false,
    _projects: null,
    _previousStates: null,
    _responseProcessors: null,
    _updatedProjects: null,
    _serverUrl: null,

    Logger: null,
    Requestor: null,
    DataSink: null,
    ConnectorConfig: null,

    Initialize: function()
    {
        this._projects = {};
        this._previousStates = {};
        this._responseProcessors = {};
        this._updatedProjects = {};
	
        this._responseProcessors = {};
        this._responseProcessors.projects = this.ParseProjects;
        this._responseProcessors.status = this.ParseStatus;
        this._responseProcessors.build = this.ParseBuild;
        this._responseProcessors.branches = this.ParseBranches;
        this._responseProcessors.tags = this.ParseTags;
    },

    Start: function()
    {
        if (this._refreshTimer != null)
        {
            this.Logger.Log("Already started.", this.Logger.LogLevels.Warning, this);
            return;
        }

        this._serverUrl = this.ConnectorConfig.ServerUrl + (this.ConnectorConfig.ServerUrl && this.ConnectorConfig.ServerUrl.endsWith("/") ? "" : "/");

        this._refreshTimer = setInterval(this.Refresh.bind(this), this.ConnectorConfig.Interval);
        this.Refresh();
    },

    Stop: function()
    {
        if (this._refreshTimer == null)
        {
            return;
        }

        clearInterval(this._refreshTimer);
        this._refreshTimer = null;
    },

    Refresh: function()
    {
        if (this._refreshing)
        {
            this.Logger.Log("Already refreshing, skipping...", this.Logger.LogLevels.Info, this);
            return;
        }

        for (var key in this._projects)
        {
            var updateInfo = this._updatedProjects[key];
            if (updateInfo && updateInfo.Status === "Updating")
            {
                this.Logger.Log("Still updating " + key + ", skipping...", this.Logger.LogLevels.Info, this);
                return;
            }

            if (!this._updatedProjects[key])
            {
                var project = this._projects[key];
                if (!this._updatedProjects[key]) { this._updatedProjects[key] = { }; }
                this._updatedProjects[key].Status = "Updating";
                this.RequestInformation("status", project.Identifier, this._serverUrl + "api/v4/projects/" + project.Identifier + "/pipelines?private_token=" + this.ConnectorConfig.Token);
                return;
            }
        }

        this.RequestInformation("projects", 0, this._serverUrl + "api/v4/projects?per_page=100&simple=false&order_by=last_activity_at&sort=desc&membership=true&private_token=" + this.ConnectorConfig.Token);
    },

    RequestFailed: function(identifier)
    {
        this._refreshing = false;
        if (identifier && this._updatedProjects[identifier])
        {
            this._updatedProjects[identifier] = "Failed";
        }
    },

    RequestInformation: function(requestType, identifier, url, callbackDone)
    {
        this._refreshing = true;

        this.Logger.Log("Requesting " + requestType + " (" + identifier + ")...", this.Logger.LogLevels.Debug, this);

        var self = this;
        this.Requestor.Get(url,
            function(data)
            {
                self._refreshing = false;
                App.ShowWarning(false);

                var processor = self._responseProcessors[requestType];
                if (processor)
                {
                    processor.call(self, identifier, data);
                }
                else
                {
                    self.Logger.Log("No response processor for " + requestType + " found.", self.Logger.LogLevels.Error, self);
                }

                if (callbackDone)
                {
                    callbackDone(data);
                }
            },
            function(jqXhr, textStatus, errorThrown)
            {
                self.RequestFailed.call(self, identifier);

                // TODO: Localize
                var message = errorThrown || "Check Server URL or press F12 to check the console.";
                App.ShowWarning(true, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ": " + message);
                self.Logger.Log(message, self.Logger.LogLevels.Warning, self);
            },
            function(data, textStatus)
            {
                self._refreshing = false;
                if (requestType === "build" || requestType === "status" && self._projects[identifier].Status === "NoBuilds")
                {
                    if (!self._updatedProjects[identifier]) { self._updatedProjects[identifier] = { }; }
                    self._updatedProjects[identifier].Status = textStatus === "success" ? "Updated" : "Failed";
                }
            }
        );
    },

    ParseProjects: function(identifier, data)
    {
        this.Logger.Log("Parsing project list...", this.Logger.LogLevels.Debug, this);

        var projects = {};
        for (var key in data)
        {
            var element = data[key];
            var project = this._projects[element.id] ? this._projects[element.id] : { };
            projects[element.id] = project;
            project.Identifier = element.id;
            project.Name = element.name;
            project.Status = project.Status ? project.Status : "Unknown";
            project.Image = element.avatar_url ? element.avatar_url + "?private_token=" + this.ConnectorConfig.Token : null;
            project.Url = element.web_url ? element.web_url.trim() + "/pipelines" : null;
            project.References = project.References || {};
        }
        this._projects = projects;
        this._updatedProjects = { };

        this.DataSink.Update(this._projects, null, this);
    },

    // eslint-disable-next-line complexity
    ParseStatus: function(identifier, data)
    {
        // TODO: reduce complexity
        var project = this._projects[identifier];
        this.Logger.Log("Parsing status of " + project.Name + " (" + project.Identifier + ")...", this.Logger.LogLevels.Debug, this);

        var lastStatus = null;

        // Check if one of the builds is running or has a failed important ref
        var importantRefs = this.ConnectorConfig.ImportantRefs;
        var checkedRefs = { };
        var failedImportantItem = null;
        var failedImportantRef = null;
        var countSuccessful = 0.0;
        var countFailed = 0.0;
        for (var index in data)
        {
            var item = data[index];
            if (item.status === "skipped") { continue; }
            if (item.status === "success") { countSuccessful++; }
            if (item.status === "failed") { countFailed++; }

            var reference = item.ref;
            if (!lastStatus && data[index].status === "running")
            {
                lastStatus = item;
            }

            if (!failedImportantItem)
            {
                for (var importantRef in importantRefs)
                {
                    if (importantRef === reference || importantRef.endsWith("*") && reference.startsWith(importantRef.substr(0, importantRef.length - 1)))
                    {
                        if (item.status === "failed"
                                && !checkedRefs[reference]
                                && project.References[reference] !== "Deleted")
                        {
                            failedImportantItem = item;
                            failedImportantRef = reference;
                        }
                        checkedRefs[reference] = true;
                    }
                }
            }
        }

        // No running build found, select the latest one that is not skipped
        if (!lastStatus)
        {
            for (var dataIndex in data)
            {
                if (data[dataIndex].status !== "skipped")
                {
                    lastStatus = data[dataIndex];
                    break;
                }
            }
        }

        if (!failedImportantItem)
        {
            project.ImportantRefStatus = "Success";
        }
        else
        {
            project.FailedImportantRef = failedImportantRef;
            switch (project.References[failedImportantRef])
            {
                case "Deleted":
                    project.ImportantRefStatus = "Success";
                    break;
                case "Branch":
                case "Tag":
                    project.ImportantRefStatus = "Failed";
                    break;
                default:
                    project.ImportantRefStatus = "PendingFailed";
                    this.CheckFailedImportantRef(project);
                    break;
            }
        }

        var countAll = countSuccessful + countFailed;
        project.Weather = countAll <= 0 ? -1.0 : countSuccessful / countAll;

        if (!lastStatus)
        {
            project.Status = "NoBuilds";
            this.DataSink.Update(this._projects, identifier, this);
            return;
        }

        var statusString = lastStatus.status;
        if (!statusString)
        {
            project.Status = "Unknown";
            this.DataSink.Update(this._projects, identifier, this);
            return;
        }

        project.Status = statusString.charAt(0).toUpperCase() + statusString.slice(1);
        this._previousStates[identifier] = this._previousStates[identifier] || project.Status;
        project.LastStatus = this._previousStates[identifier];
        if (project.Status !== "Running" && project.Status !== "Pending")
        {
            this._previousStates[identifier] = project.Status;
        }

        project.BuildNumber = lastStatus.id;
        this.RequestInformation("build", project.Identifier, this._serverUrl + "api/v4/projects/" + project.Identifier + "/pipelines/" + project.BuildNumber + "?private_token=" + this.ConnectorConfig.Token);
    },

    ParseBuild: function(projectIdentifier, data)
    {
        var project = this._projects[projectIdentifier];
        this.Logger.Log("Parsing build status of " + project.Name + " (" + project.Identifier + ")...", this.Logger.LogLevels.Debug, this);

        project.Username = data.user.name;
        project.Avatar = data.user.avatar_url;
        project.Duration = data.duration;
        project.Reference = data.ref;
        project.Coverage = data.coverage;
        project.StartedAt = data.started_at || data.created_at;
        project.FinishedAt = data.finished_at;

        this.DataSink.Update(this._projects, projectIdentifier, this);
    },

    ParseBranches: function(projectIdentifier, data)
    {
        var project = this._projects[projectIdentifier];
        this.Logger.Log("Parsing branches of " + project.Name + " (" + project.Identifier + ")...", this.Logger.LogLevels.Debug, this);
        this.ParseReferences(project, data, "Branch");
    },

    ParseTags: function(projectIdentifier, data)
    {
        var project = this._projects[projectIdentifier];
        this.Logger.Log("Parsing tags of " + project.Name + " (" + project.Identifier + ")...", this.Logger.LogLevels.Debug, this);
        this.ParseReferences(project, data, "Tag");
    },

    ParseReferences: function(project, data, type)
    {
        for (var index in data)
        {
            var reference = data[index].name;
            project.References[reference] = type;
        }
        this.CheckPendingStates(project);
    },

    CheckPendingStates: function(project)
    {
        if (project.ImportantRefStatus !== "PendingFailed")
        {
            return;
        }

        if (project.References[project.FailedImportantRef])
        {
            // The reference exists so it is failed
            project.ImportantRefStatus = "Failed";
        }
        else
        {
            // The reference has been deleted, so we ignore the failed state
            project.References[project.FailedImportantRef] = "Deleted";
            project.ImportantRefStatus = "Success";
        }

        this.DataSink.Update(this._projects, project.Identifier, this);
    },

    CheckFailedImportantRef: function(project, referenceList)
    {
        var refList = referenceList || "branches";
        var self = this;
        this.RequestInformation(refList, project.Identifier, this._serverUrl + "api/v4/projects/" + project.Identifier + "/repository/" + refList + "?private_token=" + this.ConnectorConfig.Token,
            function()
            {
                if (refList === "branches")
                {
                    self.CheckFailedImportantRef.call(self, project, "tags");
                }
            }.bind(self)
        );
    },

    CreateSnapshot: function()
    {
        return this._projects;
    },

    RestoreSnapshot: function(snapshot)
    {
        this._projects = snapshot;
    }
};